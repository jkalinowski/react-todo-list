import React from "react";
import { Container } from 'semantic-ui-react'

import TodoForm from './TodoForm';

const Todo = () => (
  <Container>
    <TodoForm />
  </Container>
)

export default Todo;
