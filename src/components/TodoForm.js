import React, { Component } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react'

import TodoList from './TodoList';

class TodoForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todoFormTitle: '',
      todoFormValue: '',
      todoList: [],
    }

    this.removeItem = this.removeItem.bind(this);
  }

  updateTodoForm(key, value) {
    this.setState({[key]: value});
  }

  addItem() {
    //check if the title and value are given
    if(this.state.todoFormTitle === '' || this.state.todoFormValue === '') {
      return false;
    }

    const newItem = {
      id: 1 + Math.random(),
      title: this.state.todoFormTitle,
      value: this.state.todoFormValue
    };
    const list = [...this.state.todoList];

    list.push(newItem);

    this.setState({
      todoFormTitle: '',
      todoFormValue: '',
      todoList: list
    });

  }

  removeItem(id) {
    //copy all items
    const list = [...this.state.todoList];

    //filter out the item
    const updatedList = list.filter(item => item.id !== id);

    //update the list
    this.setState({todoList: updatedList});
  }

  render() {
    return (
      <Segment placeholder>
          <Form>
            <Form.Input
              type="text"
              placeholder="Type item title"
              value={this.state.todoFormTitle}
              onChange={e => this.updateTodoForm('todoFormTitle', e.target.value)}
            />
            <Form.Input
              type="text"
              placeholder="Type item value"
              value={this.state.todoFormValue}
              onChange={e => this.updateTodoForm('todoFormValue', e.target.value)}
            />

            <Button content='Add new item' primary onClick={() => this.addItem()} />
          </Form>

          <TodoList list={this.state.todoList} actionHandler={this.removeItem} />
      </Segment>
    );
  }
}

export default TodoForm;
