import React, { Component } from 'react';

import { List, Button } from 'semantic-ui-react'

class TodoItem extends Component {
  render() {
    return (
      <List.Item id={this.props.id}>
        <List.Content floated='right'>
          <Button onClick={() => this.props.actionHandler(this.props.id)}>Delete</Button>
        </List.Content>

        <List.Content>
          <List.Header>{this.props.title}</List.Header>
          {this.props.value}
        </List.Content>
      </List.Item>
    );
  }
}

export default TodoItem;
