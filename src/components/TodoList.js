import React, { Component } from 'react';
import TodoItem from './TodoItem';

import { List, Segment } from 'semantic-ui-react'

class TodoList extends Component {

  render() {
    let list = (this.props.list) ? this.props.list.map(item => <TodoItem key={item.id} title={item.title} value={item.value} actionHandler={() => this.props.actionHandler(item.id)}/>) : null;

    return (
      <Segment>
       <List divided relaxed>
         {list}
       </List>
     </Segment>
    );
  }
}

export default TodoList;
